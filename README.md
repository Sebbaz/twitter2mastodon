# twitter2mastodon

twitter2mastodon is a Python script that copy posts from Twitter to a Mastodon bot account.

It allows you to follow a Twitter account from Mastodon.

## Requirements

* Python3
* [tweepy](http://www.tweepy.org/) (apt install python3-tweepy)
* [Mastodon.py](https://github.com/halcy/Mastodon.py)  (pip3 install Mastodon.py) 

## Usage

Create a config file. An interactive shell will guide you :

        $ python3 twitter2mastodon.py

Copy new posts from Twitter to Mastodon:

        $ python3 twitter2mastodon.py -c twitter2mastodon.cfg


